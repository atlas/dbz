# Potara codes

| Hex  | Potara                         |
| ---- | ------------------------------ |
| 0000 | Empty                          |
| 0001 | Attack Up 1                    |
| 0002 | Attack Up 2                    |
| 0003 | Attack Up 3                    |
| 0004 | Defense Up 1                   |
| 0005 | Defense Up 2                   |
| 0006 | Defense Up 3                   |
| 0007 | Ki Power Up 1                  |
| 0008 | Ki Power Up 2                  |
| 0009 | Ki Power Up 3                  |
| 000a | Super Up 1                     |
| 000b | Super Up 2                     |
| 000c | Super Up 3                     |
| 000d | Attack Up 2 & Defense Down 1   |
| 000e | Defense Up 2 & Super Down 1    |
| 000f | Ki Power Up 2 & Super Down 1   |
| 0010 | Super Up 2 & Power Down 1      |
| 0011 | Attack Up 3 & Super Down 1     |
| 0012 | Defense Up 3 & Attack Down 2   |
| 0013 | Ki Power Up 3 & Defense Down 1 |
| 0014 | Super Up 3 & Power Down 2      |
| 0015 | Attack Up 3 & Defense Down 2   |
| 0016 | Defense Up 3 & Power Down 1    |
| 0017 | Power Up 3 & Super Down 2      |
| 0018 | Super Up 3 & Attack Down 1     |
| 0019 |
| 001a |
| 001b |
| 001c |
| 001d |
| 001e |
| 001f |
| 0020 |
| 0021 |
| 0022 |
| 0023 | Flight                         |
| 0024 | Unleash Latent Power 1         |
| 0025 | Unleash Latent Power 2         |
| 0026 | Unleash Latent Power 3         |
| 0027 | Ki Control                     |
| 0028 | Dragon Dash Mastery            |
| 0029 | High Spot                      |
| 002a | Essence of Sight               |
| 002b | Warrior Lineage                |
| 002c | Halo                           |
| 002d | Demon Seal                     |
| 002e | Satisfying Blow                |
| 002f | Active Heart                   |
| 0030 | Indomitable Fighting Spirit    |
| 0031 | Mind Breaker                   |
| 0032 | Gigantic Power                 |
| 0033 | Rising Fighting Spirit         |
| 0034 | Secret Measures                |
| 0035 | Proof of Friendship            |
| 0036 | Dende's Healing Ability        |
| 0037 | Eternal Life                   |
| 0038 | Tension Up                     |
| 0039 | High Tension                   |
| 003a | Water Blessing                 |
| 003b | Spiritual Control              |
| 003c | Lover of Justice               |
| 003d | Evil Ambitions                 |
| 003e | Dragon Heart                   |
| 003f | Dragon Beat                    |
| 0040 | Dragon Spirit                  |
| 0041 | Confidence                     |
| 0042 | Battle Control                 |
| 0043 | Kibito's Secret Art            |
| 0044 | Persistent Threat              |
| 0045 | Miracle Master                 |
| 0046 | Exquisite Skill                |
| 0047 | Quick Return                   |
| 0048 | Heavy Pressure                 |
| 0049 | Unleash Ki                     |
| 004a | Power of Rage                  |
| 004b | Dragon Power                   |
| 004c | Latent Energy!                 |
| 004d | Fighting Spirit!               |
| 004e | Indignation!                   |
| 004f | Serious!                       |
| 0050 | Hatred of Saiyans              |
| 0051 | Rush Blast 1                   |
| 0052 | Rush Blast 2                   |
| 0053 | Rush Blast 3                   |
| 0054 | Guard Master                   |
| 0055 | Mirage                         |
| 0056 | Warrior's Will                 |
| 0057 | Evil Pride                     |
| 0058 | Perfect Guard                  |
| 0059 | Sparking Plus                  |
| 005a | Style of the Strong            |
| 005b | Emperor's Aura                 |
| 005c | Miracle Sparking               |
| 005d | Savior                         |
| 005e | Light Body                     |
| 005f | Power Body                     |
| 0060 | Ultimate Body                  |
| 0061 | Draconic Aura                  |
| 0062 | Master Strike                  |
| 0063 | Master Blast                   |
| 0064 | Master Throw                   |
| 0065 | Charged Attack                 |
| 0066 | Quick Fast Attack              |
| 0067 | Dragon Break                   |
| 0068 | Dragon Crush                   |
| 0069 | Vanishing Break                |
| 006a | Vanishing Rush                 |
| 006b | Combo Master                   |
| 006c | Quick Charge                   |
| 006d | Super Senses                   |
| 006e | Broly's Ring                   |
| 006f | Namek Power                    |
| 0070 | Earth Power                    |
| 0071 | Universal Power                |
| 0072 | Aura Charge Blue               |
| 0073 | Aura Charge Purple             |
| 0074 | Aura Charge Yellow             |
| 0075 | Aura Charge Red                |
| 0076 | Aura Charge Green              |
| 0077 | Aura Charge White              |
| 0078 | Aura Charge Pink               |
| 0079 | Aura Charge Violet             |
| 007a | Aura Charge Ultimate           |
| 007b | Aura Charge Ultimate 3         |
| 007c | Aura Charge Ultimate 4         |
| 007d | Master Roshi's Training        |
| 007e | King Kai's Training            |
| 007f | #18's Kiss                     |
| 0080 | #17's Scarf                    |
| 0081 | Medical Machine                |
| 0082 |
| 0083 |
| 0084 |
| 0085 |
| 0086 |
| 0087 |
| 0088 |
| 0089 | Launch's Support               |
| 008a | Goku Type                      |
| 008b | Vegeta Type                    |
| 008c | Gohan Type                     |
| 008d | Trunks Type                    |
| 008e | Piccolo Type                   |
| 008f | Krillin Type                   |
| 0090 | Tien Type                      |
| 0091 | Chiaotzu Type                  |
| 0092 | Yajirobe Type                  |
| 0093 | Frieza Type                    |
| 0094 | Cell Type                      |
| 0095 | Majin Buu Type                 |
| 0096 | Broly Type                     |
| 0097 | Ginyu Type                     |
| 0098 | Recoome Type                   |
| 0099 |
| 009a |
| 009b |
| 009c |
| 009d |
| 009e |
| 009f |
| 00a0 |
| 00a1 |
| 00a2 |
| 00a3 | Ultimate Warrior 1             |
| 00a4 | Ultimate Warrior 2             |
| 00a5 | Ultimate Warrior 3             |
| 00a6 | Ultimate Warrior 4             |
| 00a7 | Ultimate Warrior 5             |
| 00a8 | Ultimate Warrior 6             |
| 00a9 | Ultimate Warrior 7             |
| 00aa | Ultimate Warrior 8             |
| 00ab | Attack -25                     |
| 00ac | Attack -24                     |
| 00ad | Attack -23                     |
| 00ae | Attack -22                     |
| 00af | Attack -21                     |
| 00b0 | Attack -20                     |
| 00b1 | Attack -19                     |
| 00b2 | Attack -18                     |
| 00b3 | Attack -17                     |
| 00b4 | Attack -16                     |
| 00b5 | Attack -15                     |
| 00b6 | Attack -14                     |
| 00b7 | Attack -13                     |
| 00b8 | Attack -12                     |
| 00b9 | Attack -11                     |
| 00ba | Attack -10                     |
| 00bb | Attack -9                      |
| 00bc | Attack -8                      |
| 00bd | Attack -7                      |
| 00be | Attack -6                      |
| 00bf | Attack -5                      |
| 00c0 | Attack -4                      |
| 00c1 | Attack -3                      |
| 00c2 | Attack -2                      |
| 00c3 | Attack -1                      |
| 00c4 | Attack +0                      |
| 00c5 | Attack +1                      |
| 00c6 | Attack +2                      |
| 00c7 | Attack +3                      |
| 00c8 | Attack +4                      |
| 00c9 | Attack +5                      |
| 00ca | Attack +6                      |
| 00cb | Attack +7                      |
| 00cc | Attack +8                      |
| 00cd | Attack +9                      |
| 00ce | Attack +10                     |
| 00cf | Attack +11                     |
| 00d0 | Attack +12                     |
| 00d1 | Attack +13                     |
| 00d2 | Attack +14                     |
| 00d3 | Attack +15                     |
| 00d4 | Attack +16                     |
| 00d5 | Attack +17                     |
| 00d6 | Attack +18                     |
| 00d7 | Attack +19                     |
| 00d8 | Attack +20                     |
| 00d9 | Attack +21                     |
| 00da | Attack +22                     |
| 00db | Attack +23                     |
| 00dc | Attack +24                     |
| 00dd | Attack +25                     |
| 00de | Attack +26                     |
| 00df | Attack +27                     |
| 00e0 | Attack +28                     |
| 00e1 | Attack +29                     |
| 00e2 | Attack +30                     |
| 00e3 | Attack +31                     |
| 00e4 | Attack +32                     |
| 00e5 | Attack +33                     |
| 00e6 | Attack +34                     |
| 00e7 | Attack +35                     |
| 00e8 | Attack +36                     |
| 00e9 | Attack +37                     |
| 00ea | Attack +38                     |
| 00eb | Attack +39                     |
| 00ec | Attack +40                     |
| 00ed | Attack +41                     |
| 00ee | Attack +42                     |
| 00ef | Attack +43                     |
| 00f0 | Attack +44                     |
| 00f1 | Attack +45                     |
| 00f2 | Attack +46                     |
| 00f3 | Attack +47                     |
| 00f4 | Attack +48                     |
| 00f5 | Attack +49                     |
| 00f6 | Attack +50                     |
| 00f7 | Defense -25                    |
| 00f8 | Defense -24                    |
| 00f9 | Defense -23                    |
| 00fa | Defense -22                    |
| 00fb | Defense -21                    |
| 00fc | Defense -20                    |
| 00fd | Defense -19                    |
| 00fe | Defense -18                    |
| 00ff | Defense -17                    |
| 0100 | Defense -16                    |
| 0101 | Defense -15                    |
| 0102 | Defense -14                    |
| 0103 | Defense -13                    |
| 0104 | Defense -12                    |
| 0105 | Defense -11                    |
| 0106 | Defense -10                    |
| 0107 | Defense -9                     |
| 0108 | Defense -8                     |
| 0109 | Defense -7                     |
| 010a | Defense -6                     |
| 010b | Defense -5                     |
| 010c | Defense -4                     |
| 010d | Defense -3                     |
| 010e | Defense -2                     |
| 010f | Defense -1                     |
| 0110 | Defense +0                     |
| 0111 | Defense +1                     |
| 0112 | Defense +2                     |
| 0113 | Defense +3                     |
| 0114 | Defense +4                     |
| 0115 | Defense +5                     |
| 0116 | Defense +6                     |
| 0117 | Defense +7                     |
| 0118 | Defense +8                     |
| 0119 | Defense +9                     |
| 011a | Defense +10                    |
| 011b | Defense +11                    |
| 011c | Defense +12                    |
| 011d | Defense +13                    |
| 011e | Defense +14                    |
| 011f | Defense +15                    |
| 0120 | Defense +16                    |
| 0121 | Defense +17                    |
| 0122 | Defense +18                    |
| 0123 | Defense +19                    |
| 0124 | Defense +20                    |
| 0125 | Defense +21                    |
| 0126 | Defense +22                    |
| 0127 | Defense +23                    |
| 0128 | Defense +24                    |
| 0129 | Defense +25                    |
| 012a | Defense +26                    |
| 012b | Defense +27                    |
| 012c | Defense +28                    |
| 012d | Defense +29                    |
| 012e | Defense +30                    |
| 012f | Defense +31                    |
| 0130 | Defense +32                    |
| 0131 | Defense +33                    |
| 0132 | Defense +34                    |
| 0133 | Defense +35                    |
| 0134 | Defense +36                    |
| 0135 | Defense +37                    |
| 0136 | Defense +38                    |
| 0137 | Defense +39                    |
| 0138 | Defense +40                    |
| 0139 | Defense +41                    |
| 013a | Defense +42                    |
| 013b | Defense +43                    |
| 013c | Defense +44                    |
| 013d | Defense +45                    |
| 013e | Defense +46                    |
| 013f | Defense +47                    |
| 0140 | Defense +48                    |
| 0141 | Defense +49                    |
| 0142 | Defense +50                    |
| 0143 | Strength Enhancement           |
| 0144 | Strength Reduction             |
| 0145 | Blast Clash Weakened (Small)   |
| 0146 | Blast Clash Weakened (Medium)  |
| 0147 | Blast Clash Weakened (Large)   |
| 0148 | Absolutely Invincible Body     |
| 0149 | Go To Hell                     |
| 014a | Super Mode                     |
| 014b | Extreme Hand-to-Hand           |
| 014c | Extreme Energy Blast           |
| 014d | Extreme Blast                  |
| 014e | Extreme Defense                |
| 014f | Resistor                       |
| 0150 | Super Regeneration Cells       |
| 0151 | Complete Ki Control            |
| 0152 | God of Battle                  |
| 0153 | Fierce God                     |
| 0154 | Strength Reduction -10K        |
| 0155 | Strength Reduction -20K        |
| 0156 | Limitbreaker                   |
| 0157 | Inherited Power                |