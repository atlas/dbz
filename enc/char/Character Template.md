# Character

> All tests are done against a Goku (Early) dummy.

- [Character](#character)
  - [Base](#base)

## Base

Table 1: Melee
Base ATK: 270
Default combo: 3170

Table 2: Blasts
| Blast | Cost |       Damage | Name                       |
| ----- | ---- | -----------: | -------------------------- |
| S1    | 2    |              | Afterimage                 |
| S2    | 3    |              | Power up to the Very Limit |
| B1    | 3    |   6840(7560) | Finish Buster              |
| B2    | 3    | xxxxx(xxxxx) | Burning Storm              |
| BU    | 4    | xxxxx(xxxxx) | Lightning Sword Slash      |
