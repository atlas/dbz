# Character: Trunks (Sword)

- [Character: Trunks (Sword)](#character-trunks-sword)
  - [Trunks (Sword)](#trunks-sword)
    - [Table 1: Melee](#table-1-melee)
    - [Table 2: Special Technique](#table-2-special-technique)
    - [Table 3: Rushing Technique](#table-3-rushing-technique)
    - [Semantics](#semantics)
  - [Super Saiyan](#super-saiyan)
    - [Table 1: Melee](#table-1-melee-1)
    - [Table 2: Special Technique](#table-2-special-technique-1)
    - [Table 3: Rushing Technique](#table-3-rushing-technique-1)

## Trunks (Sword)

### Table 1: Melee

| Base ATK | Base Ki ATK |  Combo | Chg. ATK | Chg. Ki |
| -------: | ----------: | -----: | -------: | ------: |
|      270 |         260 | 7/2630 |     1180 |   *1550 |
*Unblockable

### Table 2: Special Technique

| Blast | Cost | Name                       |       Damage | Hits |
| ----: | ---- | :------------------------- | -----------: | :--- |
|  B1-1 | 2    | Afterimage                 |            0 | 0    |
|  B1-2 | 3    | Power up to the Very Limit |            0 | 0    |
|  B2-1 | 3    | Finish Buster              |   6840(7560) | 1    |
|  B2-2 | 3    | Burning Storm              |   5000(5500) | 5    |
|    BU | 4    | Lightning Sword Slash      | 12300(13550) | 5    |

### Table 3: Rushing Technique

| Input                                           | Technique    | Context     |
| :---------------------------------------------- | :----------- | ----------- |
| ![square]![triangle]                            | Flying Kick  |             |
| ![square]![square]![triangle]                   | Kiai Cannon  |             |
| ![square]![square]![square]![triangle]          | Heavy Finish |             |
| ![square]![square]![square]![square]![triangle] | Heavy Finish |             |
| ![up]+![triangle] (hold)                        | Lift Strike  | During Rush |
| ![down]+![triangle] (hold)                      | Ground Slash | During Rush |
| ![circle]                                       | Sonic Sway   | During Rush |

### Semantics

|        Semantic | Value |
| --------------: | :---- |
| Starting Health | 40k   |
|     Starting Ki | 2     |

- [x] Violent Rush ([Sparking])
- [x] [Sonic Sway] Attack

## Super Saiyan

### Table 1: Melee

| Base ATK | Base Ki ATK |  Combo | Chg. ATK | Chg. Ki |
| -------: | ----------: | -----: | -------: | ------: |
|      350 |         280 | 7/3270 |     1210 |    1680 |

### Table 2: Special Technique

| Blast | Cost | Name                 |       Damage | Hits |
| ----: | ---- | :------------------- | -----------: | :--- |
|  B1-1 | 1    | Explosive Wave       |          750 | 2    |
|  B1-2 | 3    | Burning Heart        |            0 | 0    |
|  B2-1 | 3    | Burning Attack       |   7280(8040) | 4    |
|  B2-2 | 3    | Burning Storm        |   5450(6000) | 5    |
|    BU | 5    | Shining Sword Attack | 13640(15010) | 21   |

### Table 3: Rushing Technique

| Input                                           | Technique    | Context     |
| :---------------------------------------------- | :----------- | ----------- |
| ![square]![triangle]                            | Flying Kick  |             |
| ![square]![square]![triangle]                   | Heavy Finish |             |
| ![square]![square]![square]![triangle]          | Kiai Cannon  |             |
| ![square]![square]![square]![square]![triangle] | Heavy Finish |             |
| ![up]+![triangle] (hold)                        | Lift Strike  | During Rush |
| ![down]+![triangle] (hold)                      | Ground Slash | During Rush |
| ![circle]                                       | Sonic Sway   | During Rush |

[up]: ../../img/up.png
[down]: ../../img/down.png
[left]: ../../img/left.png
[right]: ../../img/right.png
[x]: ../../img/x.png
[square]: ../../img/square.png
[triangle]: ../../img/triangle.png
[circle]: ../../img/circle.png

[Sparking]: ../training/
[Sonic Sway]: ../training/